import sqlite3 as sql
# подключаю бд и курсор для управления бд
con = sql.connect('bd_pizza.sqlite3')
cur = con.cursor()
# отправляю запрос на создание таблицы, если ее не существует
cur.execute("CREATE TABLE IF NOT EXISTS menu (prod,price)")
# создаю список, чтобы поместить в него меню
list_menu = []

# Функция main достаёт информацию из бд, передаёт её на вывод и записывает в список
def main():
	cur.execute("SELECT * FROM menu")
	menu = cur.fetchall()
	for item in menu:
		print('{0}\t\t{1}'.format(item[0],item[1]))			
		list_menu.append(item)
num = 0
# Функция zakaz создана для получения заказа от пользователя
def zakaz():
	num = 0
	# получаем заказ
	inp = input('Сделайте заказ перечисляя через запятую: ')
	# разбиваем его по запятым и получаем список с позициями в меню
	temp = inp.split(',')
	# создаю список, чтобы в нём убрать пробелы после запятой 
	last_result = []
	# в данном цикле убираю все пробелы
	for e in temp:
		last_result += e.split()
	# перебираем списки с меню и заказом, если элементы одинаковы, то в переменную
	# добавляется сумма заказа
	for i in list_menu:
		for e in last_result:
			if e == i[0]:
				num += i[1]
	# передаём на возвращение сумму заказа и список с заказом
	return num,last_result
	

def check():
	# передаём во временную переменную сумму заказа и список с заказом
	temply = zakaz()
	find = ''
	price = 0
	# в menu передаём список с заказом
	menu = temply[1]
	# создаём строку с чеком
	check_print = '''
\tПИЦЦЕРИЯ
\tДОБРО ПОЖАЛОВАТЬ
\tКАССОВЫЙ ЧЕК
	'''
	# строка с итоговым счётом
	itog = """
ИТОГ\t ={0}
	""".format(temply[0])
	# перебираем список с позициями в меню
	for e in list_menu:
	# перебираем список с заказом 
		for i in menu:
		# если элемент заказа и позиция в меню одинаковы
			if i == e[0]:
				# передаём в переменные цену и название товара
				find = e[0]
				price = e[1]
				temp_string = '''
{0}
\t1*{1}
				'''.format(find, price)
				check_print += temp_string



	print(check_print+itog)

main()
check()
